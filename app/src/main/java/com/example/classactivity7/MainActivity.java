package com.example.classactivity7;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {


    DatePicker datePicker;
    ImageButton location;
    private Tracker tracker;
    //private GpsTr
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        datePicker = findViewById(R.id.datePicker);
        location = findViewById(R.id.location);

    }
}