package com.example.classactivity7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    EditText username, password;
    Button loginBtn;
    CheckBox checkBox;
    SharedPreferences editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        loginBtn = findViewById(R.id.loginbtn);
        checkBox = findViewById(R.id.checkbtn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        editor = (SharedPreferences) getSharedPreferences("MyPref",MODE_PRIVATE);
        SharedPreferences.Editor myEdit = editor.edit();
        if (getSharedPreferences("MyPref", MODE_PRIVATE)==null) {
            checkBox.setActivated(false);
        }else
            checkBox.setActivated(true);

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean check = ((CheckBox) checkBox).isChecked();
                if (check) {
                    myEdit.putString("password", password.getText().toString());
                    myEdit.putString("username", username.getText().toString());
                    boolean coomit = myEdit.commit();
                }else {
                    myEdit.clear();
                }
            }
        });

    }
}